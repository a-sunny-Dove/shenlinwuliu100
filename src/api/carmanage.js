import axios from '@/utils/request'

// 1.获取管理车辆列表
export const getCarManageListApi = ({ page, pageSize }) => {
  return axios({
    url: '/manager/truckType/page',
    params: {
      page,
      pageSize
    }
  })
}

// 2.删除车辆
export const delCarManageApi = (id) => {
  return axios({
    method: 'delete',
    url: `/manager/truckType/${id}`
  })
}

// 3.添加车辆
export const addCarManageApi = (data) => {
  return axios({
    method: 'post',
    url: '/manager/truckType',
    data
  })
}

// 4.编辑车辆
export const editCarManageApi = (data) => {
  return axios({
    method: 'put',
    url: `/manager/truckType/${data.id}`,
    data
  })
}

// 5.编辑数据回显
export const detailsCarManageApi = (id) => {
  return axios({
    url: `/manager/truckType/${id}`
  })
}
