import axios from '@/utils/request'

// 获取运费模板列表
export const getCarriagesListApi = () => axios({
  url: '/manager/carriages'
})

// 获取新增运费模板列表
export const addCarriagesApi = (data) => axios({
  method: 'post',
  url: '/manager/carriages',
  data
})
// 删除运费模板
export const delCarriagApi = (id) => axios({
  method: 'delete',
  url: `/manager/carriages/${id}`
})
// 获取业务范围
export const getBidByIdApi = data => axios({
  url: `/manager/business-hall/scope/${data.id}/${data.type}`,
  params: {
    0: data.id
  }
})

