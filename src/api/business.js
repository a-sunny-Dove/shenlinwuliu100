import request from '@/utils/request'
//  获取订单管理数据
export const getBusiListApi = (data) => request({
  method: 'post',
  url: '/manager/order-manager/order/page',
  data
})
// 获取订单详情
export const getDetailApi = id => request({
  url: `/manager/order-manager/order/${id}`
})
// 更新订单
// export const updataDetailApi = data => request({
//   method: 'post',
//   url: `/manager/order-manager/order/${data.id}`,
//   data
// })
// 删除/更新货物
export const delOrderApi = params => request({
  url: `/manager/order-manager/cargo`,
  params
})
// 获取货物类型信息表
export const getGoodsTypeApi = () => request({
  url: '/manager/goodsType/simple'
})

//  获取运单管理数据
export const getWayBillListApi = (data) => request({
  method: 'post',
  url: '/manager/transport-order-manager/page',
  data
})
// 获取运单详情
export const getWayBillDetailApi = id => request({
  url: `/manager/transport-order-manager/${id}`
})
// 获取统计运单
export const getCountsApi = () => request({
  url: '/manager/transport-order-manager/count'
})

