import axios from '@/utils/request'
// 获取快递员分页数据
export const getExressApi = (params) => axios({
  url: '/manager/business-hall/courier/page',
  params
})
// 获取快递员作业范围分配
export const getMapCountApi = (params) => axios({
  url: `/manager/business-hall/scope/${params.id}/${params.type}`
})
// 获取司机分页数据
export const getDriverApi = (params) => axios({
  url: '/manager/driver/page',
  params
})
// 获取司机的基本信息详情
export const getDriverDetailApi = (id) => axios({
  url: `/manager/driver/${id}`
})
// 获取司机驾驶证信息
export const getDriverLicenseApi = (id) => axios({
  url: `/manager/driverLicense/${id}`
})
// 获取已经停用的车辆
export const getStopCarApi = () => axios({
  url: '/manager/unWorkingTrucks'
})
// 网点管理
// 获取业务范围
export const getBusinessScopeApi = (params) => axios({
  url: `/manager/business-hall/scope/${params.id}/${params.type}`,
  params
})
// 获取排班数据
export const getWorkListApi = (params) => axios({
  url: '/manager/work-schedulings',
  params
})
// 获取工作模式列表
export const getWorkModeListApi = () => axios({
  url: '/manager/work-patterns/all'
})
// 获取工作模式分页查询数据
export const getWokeModePageApi = (params) => axios({
  url: '/manager/work-patterns/page',
  params
})
// 根据工作模式id获取工作模式详情
export const getModeByIdApi = id => axios({
  url: `/manager/work-patterns/${id}`
})
// 删除工作模式
export const delWorkModeApi = id => axios({
  method: 'delete',
  url: `/manager/work-patterns/${id}`
})
