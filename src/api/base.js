import request from '@/utils/request'
// 获取树状机构信息
export function getDepartTree() {
  return request({
    url: '/manager/business-hall/tree'
  })
}

// 获取机构详情

export function getDepartDetail(id) {
  return request({
    url: `/manager/business-hall/${id}`
  })
}

// 根据机构id获取用户列表
export function getUserListByDepartId(params) {
  return request({
    url: `/manager/business-hall/user/page`,
    params
  })
}
// 获取城市列表
export function getCityList(params) {
  return request({
    url: `/manager/areas/children`,
    params
  })
}

export function saveDepart(data) {
  return request({
    url: '/manager/business-hall',
    method: 'post',
    data
  })
}
// 统计运输任务
export function tongjiyunshurwApi() {
  return request({
    url: '/manager/transport-order-manager/count',
    method: 'get'

  })
}

// 获取运输路线

export function getluxianApi(data) {
  return request({
    url: '/manager/transportLine/page',
    method: 'post',
    data

  })
}

// 成本配置
export function addmoneyApi(data) {
  return request({
    url: `/manager/cost-configuration-manager`,
    method: 'POST',
    data

  })
}
// 成本配置
export function getmoneyApi() {
  return request({
    url: `/manager/cost-configuration-manager`,
    method: 'get'

  })
}
// 增加线路
export function addxianluApi(data) {
  return request({
    url: `/manager/transportLine`,
    method: 'post',
    data

  })
}
// 删除线路
// /manager/transportLine/{id}
export function delxianluApi(id) {
  return request({
    url: `/manager/transportLine/${id}`,
    method: `delete`

  })
}
// 获取路线详情
export function getxianluidApi(id) {
  return request({
    url: `/manager/transportLine/${id}`,
    method: 'get'

  })
}
// 修改路线
export function changexianluidApi(id) {
  return request({
    url: `/manager/transportLine/${id}`,
    method: 'put'

  })
}

// 取件派件数据分页
export function getkuaidiApi(data) {
  return request({
    url: `/manager/pickup-dispatch-task-manager/page`,
    method: 'post',
    data

  })
}
// 获取车次详情
export function getcaridApi(id) {
  return request({
    url: `/manager/transportLine/trips${id}`,
    method: 'get'

  })
}
// 车次列表
export function getcarApi() {
  return request({
    url: `/manager/transportLine/trips`,
    method: 'get'

  })
}
// 获取运输任务分页
export function getadllyunshuApi(data) {
  return request({
    url: '/manager/transport-task-manager/page',
    method: 'POST',
    data
  })
}

