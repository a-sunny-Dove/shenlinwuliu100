import axios from '@/utils/request'

// 1.获取车辆登记数据
export const getCarRegisterListApi = (data) => {
  return axios({
    method: 'post',
    url: '/manager/truck-return-register/pageQuery',
    data
  })
}

// 2.获取起始地机构tree数据
export function getDepartTreeApi() {
  return axios({
    url: '/manager/business-hall/tree'
  })
}

// 3.回车登记数据
export const getCarReceiptApi = (id) => {
  return axios({
    url: `/manager/truck-return-register/detail/${id}`
  })
}
