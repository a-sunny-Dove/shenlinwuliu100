import axios from '@/utils/request'

// 1.获取车辆列表数据
export const getCarListApi = (params) => {
  return axios({
    url: '/manager/truck/page',
    params
  })
}

// 2.获取车辆类型
export const getCarTypeApi = () => {
  return axios({
    url: '/manager/truckType/simple'
  })
}

// 3.添加车辆
export const addCarApi = (data) => {
  return axios({
    method: 'post',
    url: '/manager/truck',
    data
  })
}

// 4.可用车辆
export const usableCarApi = () => {
  return axios({
    url: '/manager/workingTrucks'
  })
}

// 5.启用车辆
export const enableCarApi = (id) => {
  return axios({
    method: 'put',
    url: `/manager/enable/${id}`
  })
}

// 6.停用车辆
export const outCarApi = (id) => {
  return axios({
    method: 'put',
    url: `/manager/disable/${id}`
  })
}

// 7.获取车辆数量
export const carNumApi = () => {
  return axios({
    url: '/manager/count'
  })
}

// 8.获取车辆详细
export const getCarTruckApi = (id) => {
  return axios({
    url: `/manager/truck/${id}`
  })
}

// 9.获取驾驶证信息
export const getCarLicenseApi = (id) => {
  return axios({
    url: `/manager/truck/${id}/license`
  })
}

