import Layout from '@/layout'

export default {
  path: '/user',
  component: Layout,
  meta: {
    title: '员工管理',
    icon: 'el-icon-s-check'
  },
  children: [{
    path: 'courier',
    meta: {
      title: '快递员管理'
    },
    component: () => import('@/views/user/courier')
  }, {
    path: 'driver',
    meta: {
      title: '司机管理'
    },
    component: () => import('@/views/user/driver')
  }, {
    path: 'workday',
    meta: {
      title: '排班管理'
    },
    component: () => import('@/views/user/workday')
  }, {
    path: 'mapCount/:id?',
    meta: {
      title: '作业范围分配'
    },
    component: () => import('@/views/user/courier/mapCount'),
    hidden: true
  }, {
    path: 'driverDetails/:id?',
    meta: {
      title: '司机详情'
    },
    component: () => import('@/views/user/driver/driverDetails'),
    hidden: true
  }, {
    path: 'workSetting',
    meta: {
      title: '排班设置'
    },
    component: () => import('@/views/user/workday/workSetting'),
    hidden: true
  }
  ]
}
