import Layout from '@/layout'
export default {
  path: '/business',
  component: Layout,
  meta: {
    title: '业务管理',
    icon: 'el-icon-s-opportunity'
  },
  children: [{
    path: 'order',
    meta: {
      title: '订单管理'
    },
    component: () => import('@/views/business/order')
  },
  // 订单详情
  {
    path: 'detail/:id?',
    name: 'detail',
    meta: {
      title: '查看详情'
    },
    component: () => import('@/views/business/detail'),
    hidden: true // 在侧边隐藏路由
  },
  {
    path: 'waybill',
    meta: {
      title: '运单管理'
    },
    component: () => import('@/views/business/waybill')
  },
  // 运单详情
  {
    path: 'waybilldetail/:id?',
    name: 'waybilldetail',
    meta: {
      title: '查看详情'
    },
    component: () => import('@/views/business/waybilldetail'),
    hidden: true // 在侧边隐藏路由
  }
  ]
}
