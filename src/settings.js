module.exports = {

  title: '神领物流TMS管理系统',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: true, // 头部导航固定
  // fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: true
}

/* <el-table
v-loading="loading"
stripe
:data="tableData"
style="width: 100%;"
>

<el-table-column
  width="50px"
  prop="number"
  type="expand"
>

  <template>
    <el-table
      style="width: 100%"
      height="250"
    >

      <el-table-column
        prop="address"
        label="司机安排"
        width="200"
      />
      <el-table-column
        prop="zip"
        label="操作"
        width="300"
      />
    </el-table>
  </template>
</el-table-column>

<el-table-column
  width="190px"
  prop="number"
  label="线路编号"
  highlight-current-row="true"
/>
<el-table-column
  prop="id"
  label="线路名称"
  width="190px"
/>

<el-table-column
  prop="startOrganName"
  label="起始地机构"
  width="100px"
/>
<el-table-column
  prop="endOrganName"
  label="目的地机构"
  width="100px"
/>

</el-table> */
